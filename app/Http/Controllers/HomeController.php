<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Fonction qui retourne la page d'accueil
     *
     * @return View
     */
    public function index()
    {
        return view('layouts.app');
    }

    /**
     * Fonction qui retourne la page a propos
     *
     * @return View
     */
    public function aPropos()
    {
        $nom = "M. DIENE";
        return view('a_propos', compact('nom'));
    }
}
